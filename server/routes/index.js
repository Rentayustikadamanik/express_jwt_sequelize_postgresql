import express from 'express';
const router = express.Router();
// require('../config/')(passport)

import Users from '../controllers/user';
import DataDesas from '../controllers/datadesa';
import DataKarbons from '../controllers/datakarbon';
import PetaKarbons from '../controllers/petakarbon';

export default (app) => {
    // ENSURE CONNECTION TO API
    app.get('/api', (req,res) => res.status(200).send({
        message: 'Anda berhasil backend',
    }));
    
    //---Untuk Tabel USERS---//
    //CREATE USERS 
    app.post('/api/register', Users.register);
    //GET ALL USERS
    app.get('/api/getAllUsers', Users.getAllUser);
    //GET USER BY USERNAME
    app.get('/api/getUserByUsername/:username', Users.getUserByUsername);
    //GET USER BY USERNAME
    app.get('/api/getUserByEmail/:email', Users.getUserByEmail);
    //LOGIN
    app.post('/api/login', Users.login);
    //router khusus User yang sudah punya token (sudah login)
    // app.get('/api/private', Users.isAuthentication, function(req,res,next) {
    //     token = req.headers.authoriz
    // })

    //---Untuk Tabel DATADESA---//
    //GET ALL DATADESA
    app.get('/api/getAllDataDesa', Users.isAuthentication, DataDesas.getAllDataDesa);
    //GET DATADESA BY ID
    app.get('/api/getDataDesaById/:id', Users.isAuthentication, DataDesas.getDataDesaById);
    //CREATE DATADESA
    app.post('/api/createDataDesa', Users.isAuthentication, DataDesas.createDataDesa);
    //UPDATE DATADESA BY ID
    app.put('/api/updateDataDesaById/:id', Users.isAuthentication, DataDesas.updateDataDesaById);
    //DELETE DATADESA BY ID
    app.delete('/api/deleteDataDesaById/:id', Users.isAuthentication, DataDesas.deleteDataDesaById);

    //---Untuk Tabel DATAKARBON---//
    //GET ALL DATAKARBON
    app.get('/api/getAllDataKarbon', Users.isAuthentication, DataKarbons.getAllDataKarbon);
    //GET DATAKARBON BY ID
    app.get('/api/getDataKarbonById/:id', Users.isAuthentication, DataKarbons.getDataKarbonById);
    //CREATE DATAKARBON
    app.post('/api/createDataKarbon', Users.isAuthentication, DataKarbons.createDataKarbon);
    //UPDATE DATAKARBON
    app.put('/api/updateDataKarbonById/:id', Users.isAuthentication, DataKarbons.updateDataKarbonById);
    //DELETE DATAKARBON BY ID
    app.delete('/api/deleteDataKarbonById/:id',Users.isAuthentication, DataKarbons.deleteDataKarbonById);

    //---Untuk Tabel PETAKARBON---//
    //GET ALL PETAKARBON
    app.get('/api/getAllPetaKarbon', Users.isAuthentication, PetaKarbons.getAllPetaKarbon);
    //GET PETAKARBON BY ID
    app.get('/api/getPetaKarbonById/:id', Users.isAuthentication, PetaKarbons.getPetaKarbonById);    
    //CREATE PETAKARBON
    app.post('/api/createPetaKarbon', Users.isAuthentication, PetaKarbons.createPetaKarbon);
    //UPDATE PETAKARBON
    app.put('/api/updatePetaKarbonById/:id', Users.isAuthentication, PetaKarbons.updatePetaKarbonById);
    //DELETE PETAKARBON BY ID
    app.delete('/api/deletePetaKarbonById/:id', Users.isAuthentication, PetaKarbons.deletePetaKarbonById);

}