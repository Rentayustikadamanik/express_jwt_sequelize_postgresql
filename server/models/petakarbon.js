'use strict';
module.exports = (sequelize, DataTypes) => {
  const PetaKarbon = sequelize.define('PetaKarbon', {
    namaKecamatan: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan nama kecamatan'
      }
    },
    perkotaan: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan jumlah dalam perkotaan'
      },
    },
    perdesaan: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan jumlah dalam perdesaan'
      },
    },
    total: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan jumlah total emisi'
      },
    },
  }, {});
  PetaKarbon.associate = function(models) {
    // associations can be defined here
  };
  return PetaKarbon;
};