'use strict';
module.exports = (sequelize, DataTypes) => {
  const DataDesa = sequelize.define('DataDesa', {
    fotoDesa: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan foto desa'
      }
    },
    waktuKegiatan: {
      type: DataTypes.DATE,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan waktu kegiatan'
      },
    },
    peserta: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan peserta kegiatan'
      },
    },
    hasil: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan hasil kegiatan'
      },
    },
  }, {});
  DataDesa.associate = function(models) {
    // associations can be defined here
  };
  return DataDesa;
};