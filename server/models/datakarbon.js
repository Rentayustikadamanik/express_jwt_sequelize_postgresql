'use strict';
module.exports = (sequelize, DataTypes) => {
  const DataKarbon = sequelize.define('DataKarbon', {
    namaNegara: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan nama negara'
      }
    },
    karbonTotal: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan total karbon'
      },
    },
    karbonPerKapita: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan karbon per kapita'
      },
    },
  }, {});
  DataKarbon.associate = function(models) {
    // associations can be defined here
  };
  return DataKarbon;
};