const bcrypt = require('bcrypt');

export default (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    namaLengkap: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan nama anda'
      }
    },
    username: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan username anda'
      },
      unique: {
        args: true,
        msg: 'Username sudah terdaftar'
      },
    },
    email: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan alamat email anda'
      },
      unique: {
        args: true,
        msg: 'Alamat email sudah terdaftar'
      },
      validate: {
        isEmail: {
          args: true,
          msg: 'Format email harus nama@example.com'
        },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan password'
      },
      validate: {
        isNotShort: (value) => {
          if(value.length < 8) {
            throw new Error('Password harus minimal 8 karakter');
          }
        },
      },
    },
    fotoUser: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Silakan masukkan foto anda'
      }
    },
  },{});
  User.beforeSave((user, options)=> {
    if(user.changed('password')){
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    }
  });
  User.prototype.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(error, isMatch){
      if(error){
        return cb(error)
      }
      cb(null, isMatch);
    });
  };
  User.associate = (models) => {
    // // hubungan model user
    // User.hasMany(models.Book, {
    //   foreignKey: 'userId',
    // });
  };
  // User.beforeCreate((user) => {
  //   user.password = bcrypt.hashSync(user.password, 10); //SaltRound
  // })
  return User;
};