import model, {Sequelize} from '../models';

const {PetaKarbon} = model;
const Op = Sequelize.Op;

class PetaKarbons {
    static createPetaKarbon(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {namaKecamatan, perkotaan, perdesaan, total} = req.body;
            return PetaKarbon
                .create({
                    namaKecamatan,
                    perkotaan,
                    perdesaan,
                    total
                })
                .then(petaKarbon=>res.status(201).send({
                    success: true,
                    message: 'Peta Karbon berhasil ditambahkan',
                    petaKarbon
                })
            );
        }
    }

    static getAllPetaKarbon(req,res) {
        var token = req.headers.authorization;
        if(token) {
            return PetaKarbon
            .findAll()
            .then(petaKarbon=>res.status(200).json(petaKarbon));
        }
    }

    static getPetaKarbonById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            return PetaKarbon
            .findOne({
                where: {
                    id: req.params.id
                }
            })
            .then((petaKarbon)=>{
                if(!petaKarbon){
                    return res.status(404).send({
                        message: "Peta karbon tidak ditemukan"
                    });
                }
                return res.status(200).json(petaKarbon)
            })
            .catch(error=>res.status(404).send(error));
        }
    }

    static updatePetaKarbonById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {namaKecamatan, perkotaan, perdesaan, total} = req.body;
            return PetaKarbon
                .findOne({
                    where:{
                        id: req.params.id
                    }
                })
                .then(petaKarbon=>{
                    if(!petaKarbon){
                        return res.status(404).send({
                            message: "Peta Karbon berhasil diperbaharui"
                        });
                    }
                    return petaKarbon
                        .update({
                            namaKecamatan: namaKecamatan || petaKarbon.namaKecamatan,
                            perkotaan: perkotaan || petaKarbon.perkotaan,
                            perdesaan: perdesaan || petaKarbon.perdesaan,
                            total: total || petaKarbon.total,
                        })
                        .then((petaKarbon)=>res.status(200).send(petaKarbon))
                        .catch((error)=>res.status(400).send(error));
                })
                .catch(error=>res.status(400).send(error));
        }
    }

    static deletePetaKarbonById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            return PetaKarbon
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then((petaKarbon)=>{
                    if(!petaKarbon){
                        return res.status(404).send({
                            message: "Data Peta Karbon tidak ditemukan"
                        });
                    }
                    return petaKarbon
                        .destroy()
                        .then(()=>res.status(200).send({
                            message:"Data Peta Karbon berhasil dihapus"
                        }))
                        .catch(error=>res.status(400).send(error))
                })
                .catch(error=>res.status(400).send(error));
        }
    }
}

export default PetaKarbons;