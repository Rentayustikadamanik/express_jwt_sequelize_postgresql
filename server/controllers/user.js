import model, { Sequelize } from '../models/';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const {User} = model;
const Op = Sequelize.Op;

class Users {
    static isAuthentication(req, res, next) {
        var token = req.body.token || req.query.token || req.headers.authorization
        if(token) { //jika ada token
            jwt.verify(token, 'JWTBVT', function(error, decoded){ //jwt melakukan verifikasi
                if(error){ //jika ada error
                    res.json({
                        message: "Token tidak valid" //jwt melakukan respon gagal terhadap verifikasi
                    })
                } else {//jika berhasil
                    req.decoded = decoded; //menyimpan decoded ke dalam req.decoded
                    next();
                }
            });
        } else { //jika token tidak ada
            return res.status(403).send({
                message: "Unauthorized User." //melakukan respon kalau tidak ada token
            })
        }
    }
    
    static register(req,res) {
        const {namaLengkap, username, email, password, fotoUser} = req.body;
        return User
            .create({
                namaLengkap,
                username,
                email,
                password, 
                fotoUser
            })
            .then(userData=> 
                res.status(201).send({
                    success: true,
                    message: 'User berhasil didaftarkan',
                    userData
                })
            )
            .catch((error) => {
                res.status(500).json({
                    message: error.message
                })
            });
    }

    static getAllUser(req, res) {
        return User
            .findAll()
            .then(users=>res.status(200).json(users));
    }

    static getUserByUsername(req,res) {
        return User
            .findOne({
                where: {
                    username: req.params.username
                }
            })
            .then((userData)=>{
                if(!userData){
                    return res.status(404).send({
                        message: "Data User tidak ditemukan",
                    });
                }
                return res.status(200).json(userData)
            })
            .catch(error=>res.status(404).send(error));
    }

    static getUserByEmail(req,res) {
        return User
            .findOne({
                where: {
                    email: req.params.email
                }
            })
            .then((userData)=>{
                if(!userData){
                    return res.status(404).send({
                        message: "Data User tidak ditemukan",
                    });
                }
                return res.status(200).json(userData)
            })
            .catch(error=>res.status(404).send(error));
    }

    static login(req,res) {
        return User
            .findOne({
                where: {
                    [Op.or] : [
                        {username: req.body.namaPengguna},
                        {email: req.body.namaPengguna}
                    ],
                }
            })
            .then(userData=> {
                if(!userData){ //jika alamat email maupun username tidak ditemukan
                    return res.status(404).send({
                        message: "Data pengguna tidak ditemukan"
                    })
                } else { // apabila salah satu email maupun username ada yang cocok
                    if(bcrypt.compareSync(req.body.password, userData.password)) { //apabila data masukan password sama nilai dan tipe data dengan data password user di database
                        var token = jwt.sign(userData.toJSON(), 'JWTBVT', { //melakukan generate token di jwt
                            algorithm: 'HS256'  //agoritma yang digunakan
                        });
                        res.json({
                            message: "Berhasil Login", 
                            token: token
                        })
                    } else { //apabila password salah
                        res.json({
                            message: "Password yang dimasukkan salah"
                        })
                    }
                }
                return res.status(200).json(userData)
            })
            // .catch(error => {
            //     res.status(500).send('Error -> '+error)
            // })
    }
}

export default Users;