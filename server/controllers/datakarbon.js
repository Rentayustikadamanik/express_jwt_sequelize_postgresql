import model, {Sequelize} from '../models';

const {DataKarbon} = model;
const Op = Sequelize.Op;

class DataKarbons {
    static createDataKarbon(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {namaNegara, karbonTotal, karbonPerKapita} = req.body;
            return DataKarbon
                .create({
                    namaNegara,
                    karbonTotal,
                    karbonPerKapita
                })
                .then(dataKarbon=>res.status(201).send({
                    success: true,
                    message: 'Data Karbon berhasil ditambahkan',
                    dataKarbon
                })
            );
        }
    }

    static getAllDataKarbon(req, res) {
        var token = req.headers.authorization;
        if(token) {
            return DataKarbon
            .findAll()
            .then(dataKarbon=>res.status(200).json(dataKarbon));
        }
    }

    static getDataKarbonById(req, res) {
        var token = req.headers.authorization;
        if(token) {
            return DataKarbon
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then((dataKarbon)=>{
                    if(!dataKarbon){
                        return res.status(404).send({
                            message: "Data karbon tidak ditemukan"
                        });
                    }
                    return res.status(200).json(dataKarbon)
                })
                .catch(error=>res.status(404).send(error));
        }
    }

    static updateDataKarbonById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {namaNegara, karbonTotal, karbonPerKapita} = req.body;
            return DataKarbon
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then(dataKarbon=>{
                    if(!dataKarbon){
                        return res.status(404).send({
                            message: "Data Karbon tidak ditemukan"
                        });
                    }
                    return dataKarbon
                        .update({
                            namaNegara: namaNegara || dataKarbon.namaNegara,
                            karbonTotal: karbonTotal || dataKarbon.karbonTotal,
                            karbonPerKapita: karbonPerKapita || dataKarbon.karbonPerKapita,
                        })
                        .then((dataKarbon)=>res.status(200).send(dataKarbon))
                        .catch(error=>res.status(400).send(error));
                })
                .catch(error=>res.status(400).send(error));
        }
    }

    static deleteDataKarbonById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            return DataKarbon
                .findOne({
                    where:{
                        id: req.params.id
                    }
                })
                .then(dataKarbon=>{
                    if(!dataKarbon) {
                        return res.status(404).send({
                            message: "Data Korban tidak ditemukan"
                        });
                    }
                    return dataKarbon
                        .destroy()
                        .then(()=>res.status(200).send({
                            message: "Data Karbon berhasil dihapus"
                        }))
                        .catch(error=>res.status(404).send(error))
                })
                .catch(error=>res.status(404).send(error))
        }
    }
}

export default DataKarbons