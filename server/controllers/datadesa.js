import model, {Sequelize} from '../models';

const {DataDesa} = model;

class DataDesas {
    static createDataDesa(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {fotoDesa, waktuKegiatan, peserta, hasil} = req.body;
            return DataDesa
                .create({
                    fotoDesa, 
                    waktuKegiatan, 
                    peserta, 
                    hasil
                })
                .then(dataDesa=>res.status(201).send({
                    success: true,
                    message: 'Data Desa berhasil ditambahkan',
                    dataDesa
                })
            );
        }
    }

    static getAllDataDesa(req,res){
        var token = req.headers.authorization;
        if(token) {
            return DataDesa
            .findAll()
            .then(dataDesas=>res.status(200).json(dataDesas));
        }
    }

    static getDataDesaById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            return DataDesa
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then((dataDesa)=>{
                    if(!dataDesa){
                        return res.status(404).send({
                            message: "Data Desa tidak ditemukan",
                        });
                    }
                    return res.status(200).json(dataDesa)
                })
                .catch(error=>res.status(404).send(error));
        }
    }

    static updateDataDesaById(req,res) {
        var token = req.headers.authorization;
        if(token) {
            const {fotoDesa, waktuKegiatan, peserta, hasil} = req.body;
            return DataDesa
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then(dataDesa=>{
                    if(!dataDesa){
                        return res.status(404).send({
                            message: "Data Desa tidak ditemukan"
                        });
                    }
                    return dataDesa
                        .update({
                            fotoDesa: fotoDesa || dataDesa.fotoDesa,
                            waktuKegiatan: waktuKegiatan || dataDesa.waktuKegiatan,
                            peserta: peserta || dataDesa.peserta,
                            hasil: hasil || dataDesa.hasil,
                        })
                        .then((dataDesa)=>res.status(200).send(dataDesa))
                        .catch((error)=>res.status(400).send(error));
                    })
                    .catch(error=>res.status(400).send(error));
        }
    }

    static deleteDataDesaById(req,res){
        var token = req.headers.authorization;
        if(token) {
            return DataDesa
                .findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then((dataDesa)=>{
                    if(!dataDesa){
                        return res.status(404).send({
                            message: "Data Desa tidak ditemukan"
                        });
                    }
                    return dataDesa
                        .destroy()
                        .then(()=>res.status(200).send({
                            message: "Data Desa berhasil dihapus"
                        }))
                        .catch(error=>res.status(404).send(error))
                })
                .catch(error=>res.status(400).send(error))
        }
    }
}

export default DataDesas;