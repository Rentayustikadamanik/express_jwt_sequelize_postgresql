import http from 'http';
import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import router from './server/routes/index';

const app = express();

//mendefenisikan server untuk dikoneksikan
const hostname = '127.0.0.1';
const port = 3000;
const server = http.createServer(app);

app.use(logger('dev')); //log request ke console
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

router(app);

server.listen(port, hostname, () => {
    console.log('Server running pada http://',hostname,':',port,'/');
});

module.exports = app;